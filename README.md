HipChatter
=====================

### Development

To build the app, copy the following file https://bitbucket.org/squallstar/hipchatter/raw/master/HipChat/Config/Env.cs.example to the same directory without the ``.example`` part, and fill out the MatriX license.

You can request an evaluation license here: http://www.ag-software.net/matrix-xmpp-sdk/request-demo-license/

