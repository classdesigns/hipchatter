﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace HipChat.Model
{
    class MessagePopup : UserControl
    {
        MessagePopup()
        {
            HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        public static Popup GetPopup(string title, string description)
        {
            Popup p = new Popup();
            p.HorizontalAlignment = HorizontalAlignment.Stretch;

            MessagePopup message = new MessagePopup();
            p.Child = message;

            return p;
        }
    }
}
