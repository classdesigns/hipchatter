﻿using HipChat.Libs;
using Matrix;
using Matrix.Xmpp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat.Model
{
    public class ChatMessage
    {
        public bool _contactLoaded = false;
        private Contact _contact;

        public string Mid { get; set; }
        public Jid From { get; set; }
        public string Body { get; set; }
        public MessageType Type { get; set; }
        public DateTime Stamp { get; set; }

        public string FromName
        {
            get
            {
                if (FromContact != null) return _contact.Name;
                if (From.Resource != null) return From.Resource;
                return "Unknown user";
            }
        }

        public Contact FromContact
        {
            get
            {
                if (_contactLoaded == false)
                {
                    _contact = (from Contact c in HipChat.Libs.API.Current.Contacts where c.Jid == From.Bare select c).FirstOrDefault();
                    _contactLoaded = true;
                }

                return _contact;
            }
        }

        public bool IsOneToOne
        {
            get
            {
                return Type == MessageType.chat;
            }
        }

        public string StampTimeOnly
        {
            get
            {
                if (Stamp != null)
                {
                    if (Stamp.Date == DateTime.Today.Date) return Stamp.ToString("HH:mm");
                    else return Stamp.ToString("MMM dd");
                }
                return "";
            }
        }

        public bool IsVeryRecent()
        {
            if (Stamp != null && Stamp.CompareTo(DateTime.Now.AddSeconds(-45)) > 0)
            {
                return true;
            }

            return false;
        }
    }
}
